const Book = require('../models/book');

module.exports = {

  //Listing user books
  async booksList(req, res){
    try {
      const books = await Book.find().populate('user');

      return res.send({ books });
    } catch (error) {
      return res.status(400).send({ error: 'Error loading books'});
    }
  },

  //Show book details
  async bookDetails(req, res){
    try {
      const book = await Book.findById(req.params.bookId).populate('user');

      return res.send({ book });
    } catch (error) {
      return res.status(400).send({ error: 'Error loading book'});
    }
  },

  //Register a book
  async bookPost(req, res){
    try {
      const book = await Book.create({ ...req.body, user: req.userId });

      return res.send({ book });
    } catch (error) {
      return res.status(400).send({ error: 'Error creating new book'});
    }
  },

  //Update book information
  async bookPut(req, res){
    try {
      const book = await Book.findByIdAndUpdate(req.params.bookId, { 
        ...req.body
         }, { new: true });

      return res.send({ book });
    } catch (error) {
      return res.status(400).send({ error: 'Error Updating book'});
    }
  },

  //Delete a book
  async bookDelete(req, res){
    try {
      await Book.findByIdAndRemove(req.params.bookId);

      return res.send();
    } catch (error) {
      return res.status(400).send({ error: 'Error deleting book'});
    }
  }
}