const { Schema, model } = require('mongoose');

const bcrypt = require('bcryptjs');

const BookSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  autor: {
    type: String,
    required: true,
  },
  edicao: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  coverImage: {
    type: String,
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = model('Book', BookSchema);