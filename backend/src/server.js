const express = require('express');
const cors = require("cors");
const mongoose = require('mongoose');
const morgan = require('morgan');
const routes = require('./routes');

const MongoConfig = require('./config/mongoConfig');

const server = express();

//Database connection
mongoose.connect(MongoConfig.secrets, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true
});

server.use(express.json());
server.use(cors());
server.use(express.urlencoded({ extended: true }));
server.use(morgan('dev'));

server.use(routes);

server.listen(3333);