const express = require('express');

const AuthController = require('./app/controllers/authController');
const BookController = require('./app/controllers/bookController');
const authMiddleware = require('./app/middlewares/auth');

const routes = express.Router();

//User authentication
routes.post('/auth/register', AuthController.register);
routes.post('/auth/authenticate', AuthController.authenticate);

//CRUD from user created books
routes.get('/books', authMiddleware, BookController.booksList);
routes.get('/books/:bookId', authMiddleware, BookController.bookDetails);
routes.post('/books', authMiddleware, BookController.bookPost);
routes.put('/books/:bookId', authMiddleware, BookController.bookPut);
routes.delete('/books/:bookId', authMiddleware, BookController.bookDelete);

module.exports = routes;