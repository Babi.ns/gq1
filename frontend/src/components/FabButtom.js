import React, { Component } from "react";
import {  withRouter } from "react-router-dom";

import {Fab, Icon} from "@material-ui/core";

import "../styles/Global.css";

class ComponentFabButtom extends Component {

    render(){
        return (
            <div>
                <Fab color="primary" size="large" aria-label="add">
                    <Icon>add</Icon>
                </Fab>
            </div>

        );
   }
}

export default withRouter(ComponentFabButtom);