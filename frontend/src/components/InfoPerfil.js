import React, { Component } from "react";

import { Grid, Avatar} from "@material-ui/core";
import { withRouter } from "react-router-dom"

import "../styles/Global.css";

const grid={
    display:"flex",
    padding: "30px",
    marginLeft:'20px',
};

const pStyle ={
    display:'flex',
    alignItems: 'center',
    marginLeft:'80px',
    fontSize:'14pt',
}
class ComponentInfoPerfil extends Component {


    render(){
        return (
            <div>
                <Grid style={grid}>
                    <Avatar className="avatar"/>
                    <div style={pStyle}>
                        <p><b>Nome User</b></p>
                    </div>
                 </Grid>
            </div>
        );
   }
}

export default withRouter(ComponentInfoPerfil);