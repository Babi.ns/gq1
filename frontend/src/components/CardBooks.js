import React, { useEffect, useState } from "react";
import {  withRouter } from "react-router-dom";

import api from "../services/api";

import { CardContent, Typography , Card } from "@material-ui/core";


import "../screens/user/styles/Styles.css";
import "../styles/Global.css";
import bookImg from "../img/book.png";

function ComponentCardBook (){
    const [users, setUsers] = useState({books: []});
    
    useEffect(() => {
        async function loadUsers(){
          const response = await api.get("/books");

          setUsers(response.data);
        }

        loadUsers();
      }, []);
      
      console.log(users.books);
    
    return (
            
        <div>
            {users.books.length > 0 ?
                <div style={{margin:10}}>
                {users && users.books.map(book => {
                    return(
                       <Card className="card" key={book._id}>
                       <div>
                           <img className="img-card" src={bookImg}/>
                       </div>
                       <CardContent>
                           <Typography component="h5" variant="h5">
                              {book.title}
                           </Typography>
                           <Typography variant="subtitle1" color="textSecondary">
                               {book.description}
                           </Typography>
                       </CardContent>
                       
                       {/* <CardMedia
                           image="../../../img/book.png"
                           title="Live from space album cover"
                       /> */}
                   </Card>  
                    )
                })}                       
           </div>
           : (<p>Sem livros :(</p>)}
        </div>

    );
}

export default withRouter(ComponentCardBook);