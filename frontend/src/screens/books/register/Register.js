import React, { Component } from 'react';

import { TextField, Button } from "@material-ui/core";

import api from "../../../services/api";

import { withRouter } from 'react-router-dom';

import "../Styles/styles.css"
import "../../../styles/Global.css"
import bookImg from "../../../img/book.png"

class ScreeBook extends Component{
    state = {
        title: "",
        autor: "",
        edicao: "",
        description: "",
        user: "",
        error: ""
      };
  
      handleRegisterBook = async e => {
        e.preventDefault();
        const { title, autor, edicao, description, user } = this.state;
        if (!title || !autor || !edicao || !description ) {
          this.setState({ error: "Preencha todos os dados para cadastrar" });
        } else {
          try {
            await api.post("/books", { title, autor, edicao, description, user });
            this.props.history.push("/profile");
          } catch (err) {
            console.log(err);
            this.setState({ error: "Ocorreu um erro ao registrar seu livro. T.T" });
          }
        }
      };

    render(){
        return(
            <div className="container">
                <form onSubmit={this.handleRegisterBook}>
                    <div className="logo-container" style={{backgroundColor:'#35459E',color:'#fff', padding:10}}>
                        <span>Adicionar Livro</span>   
                    </div>

                    {this.state.error && <p>{this.state.error}</p>}

                    <div className="info-book-conatiner">
                        <div className="info-book">
                            <img src={bookImg} />
                            <div className="info-book-input">
                                <TextField 
                                    label="Título"  
                                    type="text" 
                                    margin="normal" 
                                    variant="outlined"
                                    onChange={e => this.setState({ title: e.target.value })} />
                                <TextField  
                                    label="Edição"  
                                    type="text" 
                                    margin="normal" 
                                    variant="outlined"
                                    onChange={e => this.setState({ edicao: e.target.value })} />
                                <TextField 
                                    label="Autor"  
                                    type="text" 
                                    margin="normal" 
                                    variant="outlined"
                                    onChange={e => this.setState({ autor: e.target.value })} />
                                <TextField 
                                    label="Descrição"  
                                    type="text" 
                                    margin="normal" 
                                    variant="outlined"
                                    onChange={e => this.setState({ description: e.target.value })} />
                            </div>
                        </div>
                    </div>

                    <div className="btn-right"  style={{ marginRight:20 }}>
                        <Button type="submit" variant="contained" color="primary">Salvar</Button>
                    </div>
                </form>
            </div>
        )
    }
}
export default withRouter(ScreeBook);