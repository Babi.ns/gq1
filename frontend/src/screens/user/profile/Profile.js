import React, { Component } from "react";
import {  withRouter, Link } from "react-router-dom";

import { Button, Icon, Avatar } from "@material-ui/core";

import { logout } from "../../../services/auth";

import "../styles/Styles.css";
import "../../../styles/Global.css";

import ComponentCardBook from "../../../components/CardBooks";
import ComponentFabButtom from "../../../components/FabButtom";

class ScreenProfile extends Component {

    handleLogout = e => {
        logout();
        this.props.history.push("/");
        };

    render(){
        return (
            <div>
                <div style={{ backgroundColor: "#35459E"}}>
                    <Button type="submit" onClick={this.handleLogout}><Icon style={{ fontSize: 40, color: "#fff" }}>close</Icon></Button>
                    {/* <Link to="/register" style={{ textDecoration: 'none' }}><Button>Criar conta</Button></Link> */}
                </div>

                <div className="logo-container">                   
                    <Avatar className="avatar"/>
                    <span><b>Nome User</b></span>
                    {/* <span><b>Perfil</b></span> */}
                </div>

                <div style={{padding: 20}}>
                    <form >

                        <div>
                            
                            <div style={{marginTop:30 }}>
                                <span className="logo-container border-top">Livros</span>
                                
                                <div className="row-card">
                                    <ComponentCardBook/>                               
                                </div>
                            </div>
                            <div className="fab-btn">
                                <Link to="/add-livro" style={{ textDecoration: 'none'}}><ComponentFabButtom/></Link>
                            </div>          
                        </div>
                    </form>
                </div>
            </div>
        );
  }
}

export default withRouter(ScreenProfile);