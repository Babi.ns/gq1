import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import api from "../../../services/api";

import { TextField, Button, Icon } from "@material-ui/core";
import "../styles/Style.css";

 class ScreenRegister extends Component {
    state = {
      username: "",
      email: "",
      password: "",
      error: ""
    };

    handleRegister = async e => {
      e.preventDefault();
      const { name, email, password } = this.state;
      if (!name || !email || !password) {
        this.setState({ error: "Preencha todos os dados para se cadastrar" });
      } else {
        try {
          await api.post("/auth/register", { name, email, password });
          this.props.history.push("/");
        } catch (err) {
          console.log(err);
          this.setState({ error: "Ocorreu um erro ao registrar sua conta. T.T" });
        }
      }
    };

  render(){ 
     return (
      <div className="container-login">
        <form 
          className="form-container" onSubmit={this.handleRegister}>
          <div className="logo-container">
            <Icon style={{ fontSize: 40, color: "#35459E" }}>menu_book</Icon>
            <span>BibComp</span>
          </div>

          {this.state.error && <p>{this.state.error}</p>}
        
          <div className="form-component">
            <TextField 
              required 
              label="Name" 
              type="text" 
              margin="normal" 
              variant="outlined"
              onChange={e => this.setState({ name: e.target.value })} 
            />
            <TextField 
              required 
              label="Email" 
              type="text" 
              margin="normal" 
              variant="outlined"
              onChange={e => this.setState({ email: e.target.value })}  
            />
            <TextField 
              required 
              label="Senha" 
              type="password" 
              margin="normal" 
              variant="outlined"
              onChange={e => this.setState({ password: e.target.value })} 
            />
            <div className="btn-container">
              {/* <Link to="/register-info-user"><Button type="submit">Registrar</Button></Link> */}
              <Button type="submit">Registrar</Button>
              <Link to="/" style={{ textDecoration: 'none' }}><Button>Fazer Login</Button></Link>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default withRouter(ScreenRegister);
