import React, { Component } from "react";
import { Link, withRouter  } from "react-router-dom";

import { TextField, Button, Icon } from "@material-ui/core";
import "../styles/Style.css";

import api from "../../../services/api";
import { login } from "../../../services/auth";

class ScreenLogin extends Component {
  state={
    email: "",
    password: "",
    error: ""
  };

  hendleLogin = async e => {
    e.preventDefault();
    const { email, password } = this.state;
    if (!email || !password) {
      this.setState({ error: "Preencha e-mail e senha para continuar!" });
    } else {
      try {
        const response = await api.post("/auth/authenticate", { email, password });
        login(response.data.token);
        this.props.history.push("/profile");
      } catch (err) {
        this.setState({
          error:
            "Houve um problema com o login, verifique suas credenciais. T.T"
        });
      }
    }
  };

  render(){
    return (
      <div className="container-login">
        <form className="form-container" onSubmit={this.hendleLogin}>
          <div className="logo-container">
            <Icon style={{ fontSize: 40, color: "#35459E" }}>menu_book</Icon>
            <span>BibComp</span>
          </div>

          {this.state.error && <p>{this.state.error}</p>}
        
          <div className="form-component">
            <TextField 
            required 
            label="Email" 
            type="text" 
            margin="normal" 
            variant="outlined"
            onChange={e => this.setState({ email: e.target.value })}
            />
            <TextField 
            required 
            label="Senha"  
            type="password" 
            margin="normal" 
            variant="outlined"
            onChange={e => this.setState({ password: e.target.value })}  
            />
            <div className="btn-container">
              <Button type="submit">Entrar</Button>
              <Link to="/register" style={{ textDecoration: 'none' }}><Button>Criar conta</Button></Link>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default withRouter(ScreenLogin);