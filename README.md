# GQ1
--- BackEnd ---

-- Iniciando Backend --
- yarn init -y ou npm init -y
Para criar package.json

-- Dependêcias --
- yarn add express
- yarn add nodemon -D
- yarn add mongoose
- yarn add bcryptjs
- yarn add jsonwebtoken
- yarn add cors

--- FrontEnd ---

-- Iniciando Frontend --
- yarn create react-app nome_do_projeto ou npx create-react-app nome_do_projeto
Para instalar o pacote do React com as ferramentas pré configuradas

-- Dependêcias --
- yarn add axios